import difference from 'lodash.difference'

export type SmartcardDataKey =
  | 'NM'
  | 'NW'
  | 'OL'
  | 'BR'
  | 'PE'
  | 'PR'
  | 'VA'
  | 'DT'
  | 'PK'
  | 'SG'
  | 'CH'

export type SmartcardData = { [key: string]: string } & {
  [key in SmartcardDataKey]: string
}

const INFORMATION_SEPARATOR_TWO = '\u001E'
const INFORMATION_SEPARATOR_THREE = '\u001D'
const END_OF_TRANSMISSION = '\u0004'

const segmentsBlobExtractorRegex = {
  withInfoSepTwo: new RegExp(
    `(.+)${INFORMATION_SEPARATOR_TWO}(.+)${END_OF_TRANSMISSION}`
  ),
  withoutInfoSepTwo: new RegExp(`(.+)(NM.+)${END_OF_TRANSMISSION}`)
}

const expectedKeys: SmartcardDataKey[] = [
  'NM',
  'NW',
  'OL',
  'BR',
  'PE',
  'PR',
  'VA',
  'DT',
  'PK',
  'SG',
  'CH'
]

export function parseBangladeshNIDSmartcardBarcodeData(
  barcodeData: string
): SmartcardData {
  const data: SmartcardData = {
    _: '',
    NM: '',
    NW: '',
    OL: '',
    BR: '',
    PE: '',
    PR: '',
    VA: '',
    DT: '',
    PK: '',
    SG: '',
    CH: ''
  }

  const segmentsBlobMatch = barcodeData.match(
    barcodeData.includes(INFORMATION_SEPARATOR_TWO)
      ? segmentsBlobExtractorRegex.withInfoSepTwo
      : segmentsBlobExtractorRegex.withoutInfoSepTwo
  )

  if (!segmentsBlobMatch) {
    throw new Error('INVALID_FORMAT')
  }

  const [, _, segmentsBlob] = segmentsBlobMatch

  data._ = _

  const segments = segmentsBlob.split(INFORMATION_SEPARATOR_THREE)

  const foundKeys: SmartcardDataKey[] = []

  for (const segment of segments) {
    const key = segment.slice(0, 2) as SmartcardDataKey
    const value = segment.slice(2)

    data[key] = value

    foundKeys.push(key)
  }

  const missingKeys = difference(expectedKeys, foundKeys)

  if (missingKeys.length === 0) {
    return data
  }

  for (const missingKey of missingKeys) {
    for (const foundKey of foundKeys) {
      const foundValue = data[foundKey]

      const missingValueExtractorRegex = new RegExp(`(.+).${missingKey}(.+)`)

      const valueMatch = foundValue.match(missingValueExtractorRegex)

      if (!valueMatch) {
        continue
      }

      const [, updatedFoundValue, extractedMissingValue] = valueMatch

      data[foundKey] = updatedFoundValue
      data[missingKey] = extractedMissingValue

      foundKeys.push(missingKey)

      break
    }
  }

  return data
}
