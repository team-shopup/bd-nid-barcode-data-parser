import { parseBangladeshNIDSmartcardBarcodeData } from './smartcard'

const healthyData = {
  withInfoSepTwo: `<]?NMMR. JOHN DOENW1234567890OL19871234567890123BR19870630PE1234PR1234VA123456DT20150630PK24SGd2h5IHlvdSB0cnluYSBkZWNvZGUgdGhpcz8gd2h5Pw==CH01BC456`,
  withoutInfoSepTwo: `<]?NMMR. JOHN DOENW1234567890OL19871234567890123BR19870630PE1234PR1234VA123456DT20150630PK24SGd2h5IHlvdSB0cnluYSBkZWNvZGUgdGhpcz8gd2h5Pw==CH01BC456`
}

describe('works with healthy data', () => {
  test('works with healthy data w/ INFORMATION_SEPARATOR_TWO', () => {
    const parsedData = parseBangladeshNIDSmartcardBarcodeData(
      healthyData.withInfoSepTwo
    )

    expect(parsedData).toMatchSnapshot()
  })

  test('works with healthy data w/o INFORMATION_SEPARATOR_TWO', () => {
    const parsedData = parseBangladeshNIDSmartcardBarcodeData(
      healthyData.withoutInfoSepTwo
    )

    expect(parsedData).toMatchSnapshot()
  })
})

// `\u001d` before `NW` was swapped with `\u004d` (Unicode for `M`)
const unhealthyData = `<]?NMMR. JOHN DOEMNW1234567890OL19871234567890123BR19870630PE1234PR1234VA123456DT20150630PK24SGd2h5IHlvdSB0cnluYSBkZWNvZGUgdGhpcz8gd2h5Pw==CH01BC456`

test('works with unhealthy data', () => {
  const parsedData = parseBangladeshNIDSmartcardBarcodeData(unhealthyData)

  expect(parsedData).toMatchSnapshot()
})
