import { parseBangladeshNIDCardBarcodeData } from './normal'

const oldCardData = `<pin>19871234567890123</pin><name>Mr. John Doe</name><DOB>30 Jun 1987</DOB><FP>RmluZ2VyUHJpbnQ/IMKvXF8o44OEKV8vwq8=</FP><F>Right Index</F><TYPE>A</TYPE><V>2.0</V><ds>4162736f2dc2af5c5f28e38384295f2fc2af2d6c7574656c79206e6f206964656121</ds>`

test('works with old card data', () => {
  const parsedData = parseBangladeshNIDCardBarcodeData(oldCardData)

  expect(parsedData).toMatchSnapshot()
})

const temporaryCardData = `<pin>1234567890</pin><name>Mr. John Doe</name><DOB>30 Jun 1987</DOB><TYPE>A</TYPE><V>2.0</V><ds>4162736f2dc2af5c5f28e38384295f2fc2af2d6c7574656c79206e6f206964656121</ds>`

test('works with temporary card data', () => {
  const parsedData = parseBangladeshNIDCardBarcodeData(temporaryCardData)

  expect(parsedData).toMatchSnapshot()
})
