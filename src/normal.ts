export type CardKey = 'pin' | 'name' | 'DOB' | 'FP' | 'F' | 'TYPE' | 'V' | 'ds'

export type CardData = { [key: string]: string } & {
  [key in CardKey]: string
}

const segmentsExtractorRegex = /<(.+)>.+<\/\1>/g
const dataExtractorRegex = /<(.+)>(.+)<\/\1>/

export function parseBangladeshNIDCardBarcodeData(
  barcodeData: string
): CardData {
  const data: CardData = {
    pin: '',
    name: '',
    DOB: '',
    FP: '',
    F: '',
    TYPE: '',
    V: '',
    ds: ''
  }

  const segments = barcodeData.match(segmentsExtractorRegex)

  if (!segments) {
    throw new Error('INVALID_FORMAT')
  }

  for (const segment of segments) {
    const dataMatch = segment.match(dataExtractorRegex)!

    if (dataMatch) {
      const [, key, value] = dataMatch

      data[key] = value
    }
  }

  return data
}
