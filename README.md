# Bangladesh NID Barcode Data Parser

## Usage

### NID Normal Card

```js
import {
  parseBangladeshNIDCardBarcodeData
} from 'bd-nid-barcode-data-parser'

const scannedBarcodeData = `` // from PDF417 Barcode Scanner
const parsedData = parseBangladeshNIDCardBarcodeData(scannedCardData)
```

### NID Smart Card

```js
import {
  parseBangladeshNIDSmartcardBarcodeData
} from 'bd-nid-barcode-data-parser'

const scannedBarcodeData = `` // from PDF417 Barcode Scanner
const parsedData = parseBangladeshNIDCardBarcodeData(scannedCardData)
```

## Parsed Data Structure

### NID Normal Card Data

```json
{
  "pin": "",
  "name": "",
  "DOB": "",
  "FP": "",
  "F": "",
  "TYPE": "",
  "V": "",
  "ds": ""
}
```

### NID Smart Card Data

```json
{
  "_": "",
  "NM": "",
  "NW": "",
  "OL": "",
  "BR": "",
  "PE": "",
  "PR": "",
  "VA": "",
  "DT": "",
  "PK": "",
  "SG": "",
  "CH": ""
}
```
